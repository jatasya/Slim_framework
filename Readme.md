**composer commands**

composer init // to initiate composer with composer.json

composer require // to install a new package to our project.

composer update // if you can't install a particular version of a framework then type the version in composer.json and type this command in terminal to install a particular version.

**Namespaces:**
 What are namespaces? In the broadest definition namespaces are a way of encapsulating items. 
 This can be seen as an abstract concept in many places. 
 For example, in any operating system directories serve to group related files, and act as a namespace for the files within them. 
 As a concrete example, the file foo.txt can exist in both directory /home/greg and in /home/other, 
 but two copies of foo.txt cannot co-exist in the same directory. 
 In addition, to access the foo.txt file outside of the /home/greg directory, we must prepend the directory name to the file name using the directory separator to get /home/greg/foo.txt.
  This same principle extends to namespaces in the programming world.

Version 	Description
7.0.0 	Added Group use Declarations.

In the PHP world, namespaces are designed to solve two problems that authors of libraries and applications encounter when creating re-usable code elements such as classes or functions:

    Name collisions between code you create, and internal PHP classes/functions/constants or third-party classes/functions/constants.
    Ability to alias (or shorten) Extra_Long_Names designed to alleviate the first problem, improving readability of source code.

PHP Namespaces provide a way in which to group related classes, interfaces, functions and constants.  
----------------

**Debugging configuration**

xdebug in php.ini

_The below code in php.ini some of the x-debug configurations_

[Xdebug]
zend_extension=C:\xampp\php\ext\php_xdebug-2.4.0-5.6-vc11.dll _this is after including the compatible xdebug dll file in the path_
xdebug.remote_enable=1
xdebug.remote_host=127.0.0.1
xdebug.remote_port=9000
xdebug.idekey=123456

_restrat xampp_

----------------------------------------------

**In phpstorm**

1.setup a server configuration in settings

2.setup debug configuration in run dropdown

3.add a xdebug extension in your prefered browser (add idekey if required by the extension)
or 
3. add phpstorm bookmarklets in the bookmarks bar with the idekey extension.

4. then put bookmarks and debug your application by running the bookmarked page in the browser. either using anyone of the number 3 step as above



