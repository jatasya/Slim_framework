<?php
/**
 * Created by PhpStorm.
 * User: Vikas
 * Date: 6/26/2016
 * Time: 11:03 AM
 */

require __DIR__ . '/vendor/autoload.php';
date_default_timezone_set('Asia/Kolkata');

/**
 * monolog to log our changes
 */
/*use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('name');
$log->pushHandler(new StreamHandler('app.log', Logger::WARNING));
$log->addWarning('love you');*/



/*$app->get('/hello/:name', function ($name){
    echo "Hello, $name";
});*/
$app = new \Slim\Slim( array(
    /**
     * overriding the view class to Twig class
     */
    'view' => new \Slim\Views\Twig()
));

/**
 * To use Twig options
 */
$view = $app->view();
$view->parserOptions = array(
    'debug' => true
);

/**
 * In addition to all of this we also have a few helper functions which are included for both view parsers.
 * In order to start using these you can add them to their respective view parser as stated below:
 */
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

/**
 * route to my home page
 * and the relevant message.
 */
$app->get('/', function () use($app){
    $app->render('about.twig');
})->name('home');

/**
 * route to my contact form
 * and the relevant message.
 */
$app->get('/contact', function () use($app){
  $app->render('contact.twig');
})->name('contact'); // named routing

/**
 * post data routing for contact form
 */
$app->post('/contact', function () use($app){
//    var_dump($app->request->post());
    $name = $app->request->post('name') ;
    $email = $app->request->post('email');
    $msg = $app->request->post('msg');

    /**
     * sanitizing the form data
     */
    if(!empty($name) && !empty($email) && !empty($msg)){
        $cleanName = filter_var($name, FILTER_SANITIZE_STRING);
        $cleanEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
        $cleanMsg = filter_var($msg, FILTER_SANITIZE_STRING);
    }else{
        //message to user something not right
        $app->redirect('/contact');
    }

    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465)
        ->setUsername("ifocus896")
        ->setPassword("ifocus@1234")
        ->setEncryption("ssl")
    ;
    $mailer = \Swift_Mailer::newInstance($transport);

    $message = \Swift_Message::newInstance();
    $message->setSubject('Email from slimframework');
    $message->setFrom(array(
        $cleanEmail => $cleanName
    ));
    $message->setTo('ifocus896@gmail.com');
    $message->setBody($cleanMsg);

    $result = $mailer->send($message);

    if($result > 0){
        //send a message to the user
        $app->redirect('/slim_framework');
    }else{
        //message to the user that something went wrong.
        //log that there was an error
        $app->redirect('/contact');
    }



});

$app->run();